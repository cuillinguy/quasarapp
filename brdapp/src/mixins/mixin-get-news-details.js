import { firebaseAuth, firebaseDb } from 'boot/firebase'
export default {
	computed: {
		newsDetails() {
			let newsKey = this.$route.params.newsId
			 let result = {}
			 Object.keys(this.$store.state.store.newsitems).forEach(key => {
			 	if(key === newsKey){
			 	result[key] = this.$store.state.store.newsitems[key]
			 	//console.log('it matches')
			 	}
			 })

			return result

		}
	}
}