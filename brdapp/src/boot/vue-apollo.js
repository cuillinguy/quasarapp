import VueApollo from 'vue-apollo'
import ApolloClient from 'apollo-boost'

const apolloClient = new ApolloClient({
  uri: "https://us-central1-ghanabrdapp.cloudfunctions.net/graphql"//local: http://localhost:5002/ghanabrdapp/us-central1/graphql
})

const apolloProvider = new VueApollo({
  defaultClient: apolloClient
})

export default async ({ Vue, app }) => {
  Vue.use(VueApollo)
  app.apolloProvider = apolloProvider
}