// Firebase App (the core Firebase SDK) is always required and must be listed first
import * as firebase from "firebase/app";

// If you enabled Analytics in your project, add the Firebase SDK for Analytics
import "firebase/analytics";

// Add the Firebase products that you want to use
import "firebase/auth";
import "firebase/database";
import "firebase/firestore";


var firebaseConfig = {		
    apiKey: "AIzaSyAWXU-Py1WQrIdVkRfqtwWsa6Rxtv_eRoQ",		
    authDomain: "ghanabrdapp.firebaseapp.com",		
    databaseURL: "https://ghanabrdapp.firebaseio.com",		
    projectId: "ghanabrdapp",		
    storageBucket: "ghanabrdapp.appspot.com",		
    messagingSenderId: "111363321459",		
    appId: "1:111363321459:web:e1e06f630afe8877570406",		
    measurementId: "G-20FP1V97TJ"		
    };		
    // Initialize Firebase		
    let firebaseApp = firebase.initializeApp(firebaseConfig);


    // Enable offline support
    //https://dev.to/paco_ita/break-the-cache-api-limits-in-our-pwa-oo3
    firebase.firestore().enablePersistence()
    .catch(function(err) {
        if (err.code == 'unimplemented') {
            // The current browser does not support all of the
            // features required to enable persistence
        }
    });


    let firebaseAuth = firebaseApp.auth();
    let firebaseDb = firebaseApp.database();
    let firestoreDb = firebase.firestore();
    let firebaseRoot = firebase;


    firebase.analytics();

    export { firebaseAuth, firebaseDb, firestoreDb, firebaseRoot }