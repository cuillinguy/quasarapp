
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { 
        path: '/chat/:otherUserId', 
        component: () => import('pages/PageChat.vue'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: '/chat', component: () => import('pages/PageUsers.vue')
      },
      { 
        path: '', component: () => import('pages/PageSplashMain.vue'),
        meta: {
          requiresAuth: false
        }
      },
      { path: '/auth', component: () => import('pages/PageAuth.vue') },
      { path: '/sign-up', component: () => import('pages/PageSignUp.vue') },
      { path: '/settings', component: () => import('pages/UserSettingsPages/UserSettingsPage.vue')},
      { path: '/services', component: () => import('pages/ServicesPages/ServiceListPage.vue')},
      { path: '/services/service-detail/:serviceId', component: () => import('pages/ServicesPages/ServiceDetailPage.vue')},
      { path: '/alerts', component: () => import('pages/UserAlertsPages/UserAlertsPage.vue')},
      { path: '/user-services', component: () => import('pages/UserServicesPages/UserSelectedServicesPage.vue')},
      { path: '/edit-services', component: () => import('pages/UserServicesPages/EditServicesPage.vue')},
      { path: '/post-news', component: () => import('pages/BlogPages/PostBlogPage.vue')},
      { path: '/news-list', component: () => import('pages/BlogPages/BlogListPage.vue')},
      { path: '/news-list-edit', component: () => import('pages/BlogPages/NewsListPageEdit.vue')},
      { path: '/news-detail/:newsId', component: () => import('pages/BlogPages/NewsDetailPage.vue')},
      { path: '/news-edit/:newsId', component: () => import('pages/BlogPages/NewsEditPage.vue')},
      { 
        path: '/tasks', 
        component: () => import('pages/Tasks/PageToDo.vue'),
        meta: {
          requiresAuth: true
        }
      },
      { path: '/locations-test', component: () => import('pages/Mapping/GooglePlugin.vue')},
      { path: '/locations-test-google-raw', component: () => import('pages/Mapping/GoogleMaps.vue')},
      { path: '/graphql-hotdog', component: () => import('pages/graphqltest/HotDogFunction.vue')},
      { path: '/user-location', component: () => import('pages/userlocation/UserLocationRaw.vue')},
      { path: '/firestore', component: () => import('pages/firestoretest/FireStore.vue')},
      { path: '/route-list', component: () => import('pages/routelist/RouteList.vue')},
      { path: '/routes-todo', component: () => import('pages/routelist/RoutesToDo.vue')},
      { path: '/route-addresses/:roadname/:routeId', component: () => import('pages/routelist/RouteAddressesList.vue')},
      { path: '/rounds-todo', component: () => import('pages/routelist/RoundsToDo.vue')},
      {
        path: '/admin',
        name: 'admin',
        component: () => import('layouts/AdminLayout.vue'),
         meta: {
           requiresAuth: true,
           permission: 'super-user'
         },
        children: [
          {
            path: '/admin/admin-dashboard', component: () => import('pages/Admin/AdminDashboard.vue')
          }
        ]
      }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
