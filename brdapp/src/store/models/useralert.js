import { Model } from '@vuex-orm/core'

export default class UserAlerts extends Model {

    static entity = 'UserAlerts'

    static fields(){

        return {
            id: this.attr(null),
            name: this.attr(''),
            description: this.attr('')
        }

    }

}