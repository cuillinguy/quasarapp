import Vue from 'vue'
import { uid } from 'quasar'
import { firebaseAuth, firebaseDb, firestoreDb } from 'boot/firebase'
import store from '.'
//import { print_newest_available_sdk_target } from 'app/src-cordova/platforms/android/cordova/lib/android_sdk'

let messagesRef
let servicesRef
//Globally accessible State properties
const state = {
    userDetails: {},
    users: {},
    userPermissions: {},
    messages: {}, 
    services: {},
    newsitems: {},
    tasks: {},
    mapmarkers: {},
    testCollection: {}

}



const mutations = {
    setUserDetails(state, payload){
        state.userDetails = payload
    },
    setUserPermissions(state, payload){
        //state.userPermissions = payload
        console.log('set user permissions fired ' + payload.userPermissions)
        //Object.assign(state.userPermissions, payload)
        state.userPermissions = payload
        //Vue.set(state.userPermissions, payload)
    },
    removeUserPermissions(state){
        //Clear the permissions state object
        state.userPermissions = {}
    },
    addUser(state, payload){
        console.log('Add user Fired payload', payload)
        Vue.set(state.users, payload.userId, payload.userDetails)
    },
    updateUser(state, payload){
        Object.assign(state.users[payload.userId], payload.userDetails)
    },
    addMessage(state, payload) {
        Vue.set(state.messages, payload.messageId, payload.messageDetails)
    },
    clearMessages(state) {
        state.messages = {}
    },
    addService(state, payload){
        Vue.set(state.services, payload.serviceId, payload.serviceDetails)
    },
    updateService(state, payload){
        Vue.set(state.services, payload.serviceId, payload.serviceDetails)
    },
    addNews(state, payload){
        Vue.set(state.newsitems, payload.newsId, payload.newsDetails)
    },
    updateNews(state, payload){
        Vue.set(state.newsitems[payload.newsId], payload.newsDetails)
    },
    updateTask(state, payload) {
        Object.assign(state.tasks[payload.id], payload.updates)
    },
    deleteTask(state, id) {
        Vue.delete(state.tasks, id)
    },
    addTask(state, payload) {
        console.log('Add Task Mutation fired: PayloadID: ' + payload.id + ' payload.Task: ' + payload.task)
        Vue.set(state.tasks, payload.id, payload.task)
    },
    addMapMarker(state, payload){
        //console.log('addmapMarker mutation fired id: ' + payload.positionId)
        //Vue.set(state.mapmarkers, payload.position.lat, payload.position.lng)
        Vue.set(state.mapmarkers, payload.positionId, payload.positionDetails)
    },
    updateMapMarker(state, payload){
        Vue.set(state.mapmarkers[payload.id],payload.positionDetails)
    },
    addTestCollection(state, payload){
        console.log('add test collection fired')
        console.log('Payload id' + payload.id)
        console.log('Payload.data ' + payload.data)
        Vue.set(state.testCollection, payload.id, payload.data)
    }
}


const markersarray = [{
    position: {
      lat: 51.28304,
      lng: -0.74468
    }
  }, {
    position: {
      lat: 51.286442,
      lng: -0.740237
    }
}];

const actions = {
    registerUser({}, payload){
        console.log('payload' , payload)

        firebaseAuth.createUserWithEmailAndPassword(payload.email, payload.password)
        .then(response => {
            console.log(response)

            let userId = firebaseAuth.currentUser.uid
            firebaseDb.ref('users/' + userId).set({
                name: payload.name,
                email: payload.email,
                online: true
            })
        })
        .catch(function(error) {

        var errorCode = error.code;
        var errorMessage = error.message;
            if (errorCode == 'auth/weak-password') {
                alert('The password is too weak.');
            } else {
                alert(errorMessage);
            }
        console.log(error);
        });

    },
    loginUser({ commit }, payload){
        console.log('login payload' , payload)
        firebaseAuth.signInWithEmailAndPassword(payload.email, payload.password)
        .then(response => {
            console.log(response)

            //Handle their roles and permissions
            //store.dispatch('firebaseGetRolesPermissions')
            let userId = firebaseAuth.currentUser.uid
            firebaseDb.ref('users/' + userId).once('value', snapshot => {

                let userDetails = snapshot.val()
                let roleRefDetails = ''
                let roleRefDetailName = ''
                let roleRefDetailsRolePerm = ''
                let userPermissionsDetails = ''

                //Get User Role Details
                firebaseDb.ref('userroles/' + userDetails.userrole).once('value', snapshot => {
                    roleRefDetails = snapshot.val()
                    roleRefDetailName = roleRefDetails.name
                    roleRefDetailsRolePerm = roleRefDetails.roleperm
                    console.log('role ref name: ' + roleRefDetailName)
                    console.log('role perm ' + roleRefDetailsRolePerm)


                    firebaseDb.ref('roleperm/' + roleRefDetailsRolePerm).once('value', snapshot => {
                        userPermissionsDetails = snapshot.val()
                        console.log('role perm name internal: ' + userPermissionsDetails.name)


                        //set the permissions
                        commit('setUserPermissions', {
                            userId: userId,
                            userRole: roleRefDetailName,
                            userRoleId: userDetails.userrole,
                            userPermissions: roleRefDetailsRolePerm,
                            userRolePermissionsDescription: userPermissionsDetails.name
                        })
                    })

                })              

            })
            /////
        })
        .catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        if (errorCode === 'auth/wrong-password') {
            alert('Wrong password.');
        } else {
            alert(errorMessage);
        }
        console.log(error);
        });
    },
    logoutUser({ commit }){
        firebaseAuth.signOut()
        commit('removeUserPermissions')
        //console.log('LogoutUser from store')
    },
    handleAuthStateChanged({ commit, dispatch, state }) {
        firebaseAuth.onAuthStateChanged(user =>  {
            if (user) {
            //Handles Login/Out actions
            console.log('handleAuthStateChanged fired')
            let userId = firebaseAuth.currentUser.uid

            firebaseDb.ref('users/' + userId).once('value', snapshot => {

                let userDetails = snapshot.val()


                commit('setUserDetails', {
                    name: userDetails.name,
                    email: userDetails.email,
                    online: userDetails.online,
                    userId: userId
                })

                let roleRefDetails = ''
                let roleRefDetailName = ''
                let roleRefDetailsRolePerm = ''
                let userPermissionsDetails = ''

                //Get User Role Details
                firebaseDb.ref('userroles/' + userDetails.userrole).once('value', snapshot => {
                    roleRefDetails = snapshot.val()
                    roleRefDetailName = roleRefDetails.name
                    roleRefDetailsRolePerm = roleRefDetails.roleperm
                    console.log('role ref name: ' + roleRefDetailName)
                    console.log('role perm ' + roleRefDetailsRolePerm)


                    firebaseDb.ref('roleperm/' + roleRefDetailsRolePerm).once('value', snapshot => {
                        userPermissionsDetails = snapshot.val()
                        console.log('role perm name internal: ' + userPermissionsDetails.name)


                        //set the permissions
                        commit('setUserPermissions', {
                            userId: userId,
                            userRole: roleRefDetailName,
                            userRoleId: userDetails.userrole,
                            userPermissions: roleRefDetailsRolePerm,
                            userRolePermissionsDescription: userPermissionsDetails.name
                        })
                    })

                })              


            })

            dispatch('firebaseUpdateUser', {
                userId: userId,
                updates: {
                    online: true
                }
            })
            dispatch('firebaseGetUsers')
            dispatch('firebaseGetServices')
            this.$router.push('/')
            }
            else{
                //Update the status
                dispatch('firebaseUpdateUser', {
                    userId: state.userDetails.userId,
                    updates: {
                        online: false
                    }
                })
                //clear the user from state
                commit('setUserDetails', {})
                this.$router.replace('/auth')
            }
          });
    },
    firebaseUpdateUser({}, payload){
        console.log('firebase Update User (store) payload: ' + payload)
        if(payload.userId){
            firebaseDb.ref('users/' + payload.userId).update(payload.updates)
        }

    },
    firebaseGetUsers({ commit }){
        firebaseDb.ref('users').on('child_added', snapshot => {
            let userDetails = snapshot.val()
            let userId = snapshot.key
            console.log('firebase get users userDetails: ', userDetails)
            commit('addUser', { //Allows us to create a new mutation
                userId,
                userDetails
            })
        })
    firebaseDb.ref('users').on('child_changed', snapshot => {
        let userDetails = snapshot.val()
        let userId = snapshot.key
        console.log('userDetails: ', userDetails)
        commit('updateUser', { //Allows us to create a new mutation
            userId,
            userDetails
        })
    })
    },
    firebaseGetMessages({ commit, state }, otherUserId) {
		let userId = state.userDetails.userId
        messagesRef = firebaseDb.ref('chats/' + userId + '/' + otherUserId)
        console.log('MyUserId from Store: ' + userId)
		console.log('OtherUserId from Store: ' + otherUserId)
		messagesRef.on('child_added', snapshot => {
			let messageDetails = snapshot.val()
            let messageId = snapshot.key
            console.log('MessageDetails from Store: ' + messageDetails)
			commit('addMessage', {
				messageId,
				messageDetails
			})
		})
    },
    firebaseStopGettingMessages({ commit }) {
		if (messagesRef) {
			messagesRef.off('child_added')
			commit('clearMessages')
        }
        console.log('firebaseStop gettings messages - fires on leaving page')
    },
    firebaseSendMessage({}, payload) {
		console.log('payload: ', payload)
		firebaseDb.ref('chats/' + state.userDetails.userId + '/' + payload.otherUserId).push(payload.message)//pushes the data underneith the node -- our message

		payload.message.from = 'them'
		firebaseDb.ref('chats/' + payload.otherUserId + '/' + state.userDetails.userId).push(payload.message)//Message to Them
    },
    firebaseGetServices({ commit }){
        console.log('firebaseGetServices fired')
        firebaseDb.ref('services').on('child_added', snapshot => {
            let serviceDetails =  snapshot.val()
            let serviceId = snapshot.key
            console.log('firebase GetServices() fired')
            commit('addService', {
                serviceId,
                serviceDetails
            })
        })
        firebaseDb.ref('services').on('child_changed', snapshot => {
            let serviceDetails =  snapshot.val()
            let serviceId = snapshot.key
            commit('updateService', {
                serviceId,
                serviceDetails
            })
        })
    },
    addBlogPost({}, payload){
        console.log('add Blog Post from Store newsitem: ' + payload.newsitem)
        firebaseDb.ref('newsitems/').push(payload.newsitem)
    },
    updateNewsItem({}, payload){
        
        console.log('update newsItem: ' + payload)
        firebaseDb.ref('newsitems/' + payload.newsitem.newsId).update(payload.newsitem)
    },
    firebaseGetNewsItems({ commit }){
        firebaseDb.ref('newsitems').on('child_added', snapshot => {
            let newsDetails = snapshot.val()
            let newsId = snapshot.key
            console.log('firebaseGetNewsItems news id: ' + newsId)
            commit('addNews', {
                newsId,
                newsDetails
            })
          
        })

        firebaseDb.ref('newsitems').on('child_changed', snapshot => {
            let newsDetails = snapshot.val()
            let newsId = snapshot.key
            commit('updateNews', { //Allows us to create a new mutation
                newsId,
                newsDetails
            })
        })
        
    },
    getNewsItemDetails({ commit, state }, newsId) {

        let newsItem = {}
        Object.keys(state.newsitems).forEach(key => {
            if (key === newsId) {
                newsItem[key] = state.newsitems[key]
            }
        })

        console.log('NewsItem from getNewsItemDetails (store): ' + newsItem + ' NewsID: ' + newsId)
        return newsItem
    },
    updateNewsItemDetailTitleById({}, payload){

    console.log('updateNewsItemDetailTitleById payload newsId:' + payload.newsId)
    console.log('route params: ' + '-M2YeHQ2IKvo0KY4xDMz')
    //console.log('mainText:' + payload.news.mainText)
    firebaseDb.ref('newsitems/' + payload.newsId).set({
        image: payload.image,
        mainText: payload.mainText,
        subText: payload.title
    }
    )
    },
    updateTask({ commit }, payload) {
        //update firebase
        firebaseDb.ref('usertasks/' +  state.userDetails.userId + '/' + payload.id).update(payload.updates)
		commit('updateTask', payload)
    },
    deleteTask({ commit }, id) {
        commit('deleteTask', id)
    },
    addTask({ commit }, task) {
        //Add the new task
        let taskId = uid()
        let payload = {
            id: taskId,
            task: task
        }

        firebaseDb.ref('usertasks/' + state.userDetails.userId).push(payload.task)
        console.log('addTask action fired')
    },
    firebaseGetTasks({ commit }){
        console.log('firebaseGetTasks fired')

        firebaseDb.ref('usertasks/' + state.userDetails.userId).on('child_added', snapshot => {
            let task = snapshot.val()
            let taskId = snapshot.key
            console.log('firebaseGetTasks task id: ' + taskId)

            let payload = {
                id: taskId,
                task: task
            }
            commit('addTask', payload)

        })

        firebaseDb.ref('usertasks/' + state.userDetails.userId).on('child_changed', snapshot => {
            let task = snapshot.val()
            let taskId = snapshot.key

            let payload = {
                id: taskId,
                task: task
            }
            commit('updateTask', payload)

        })

    },
    updateService({ commit }, payload) {

        console.log('services updates: ' + payload.updates)
        firebaseDb.ref('services/' + payload.id).update(payload.updates)
        commit('updateService', payload)
    },
    firebaseGetMarkers({ commit }){

        console.log('firebaseGetMarkers')

        firebaseDb.ref('markerpositions/').on('child_added', snapshot => {
            let positionId = snapshot.key
            let positionDetails = snapshot.val()
                 commit('addMapMarker',
                 {
                     positionId,
                     positionDetails
                 }
             )
        });

        firebaseDb.ref('markerpositions/').on('child_changed', snapshot => {
            let positionId = snapshot.key
            let positionDetails = snapshot.val()
                 commit('updateMapMarker',
                 {
                     positionId,
                     positionDetails
                 }
             )
        });


        // markersarray.forEach(function (value, i) {
        //     console.log('%d: %s', i, value.position);
        // });

        //return this.fakeMarkers
    },
    fireStoreHelloWorld({ commit }){

       console.log('firestoreHello fired')
       const testCollection =  firestoreDb
       .collection('test')

       const helloCollection = [];

        testCollection
        .onSnapshot((testRef) => {
            const internalArray = [];
            testRef.forEach((doc) => {

                let payload = {
                    id: [doc.id],
                    data: doc.data()
                }

                console.log('payload data: ' + payload.data)
                console.log('payload.id: ' + payload.id)

                commit('addTestCollection', payload)
            });

        })
    }
}

const getters = {
 users: state => {
     //filter the users so it doesn't include the logged in user
     let usersFiltered = {}
         Object.keys(state.users).forEach(key => {
            if(key !== state.userDetails.userId){
                usersFiltered[key] = state.users[key]
            } 
         })
        return usersFiltered    
     },
     services: state => {
        
        let services = {}
        Object.keys(state.services).forEach(function(key) {
            let service = state.services[key]
            services[key] = service
        })

        return services
        
     },
    loggedInUserDetails: state => {

        let loggedIdUser = {}
        Object.keys(state.users).forEach(key => {
            if(key === state.userDetails.userId){
                loggedIdUser[key] = state.users[key]
            } 
         })

         console.log('Logged In User from Store: ' + loggedIdUser)
        return loggedIdUser
    },
    newsitems: state => {
        console.log('newsitems from store getter: ' + state.newsitems)
        return state.newsitems
    },
    tasksTodo: (state) => {
		let tasks = {}
		Object.keys(state.tasks).forEach(function(key) {
			let task = state.tasks[key]
			if (!task.completed) {
				tasks[key] = task
			}
        })
        console.log('tasks to do from store')

		return tasks
	},
	tasksCompleted: (state) => {
		let tasks = {}
		Object.keys(state.tasks).forEach(function(key) {
			let task = state.tasks[key]
			if (task.completed) {
				tasks[key] = task
			}
		})
		return tasks
    },
    getmapmarkers: (state) => {
        console.log('markers getter fired')
        return state.mapmarkers
    },
     tasksFromState: (state) => {
        let tasks = {}
		Object.keys(state.tasks).forEach(function(key) {
			let task = state.tasks[key]
			if (!task.completed) {
				tasks[key] = task
			}
        })
        console.log('tasks to do from store')

		return tasks
     }
 }


export default{
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}