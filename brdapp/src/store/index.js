import Vue from 'vue'
import Vuex from 'vuex'
import VuexORM from '@vuex-orm/core'
import store from './store'
import UserAlert from './models/useralert'
import User from './models/user'
import Post from './models/post'
import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyCaq-YHmrD587YO0qfxw4Bvc8u8hiHTZ_A",
    libraries: "places" // necessary for places input
  }
});

Vue.use(Vuex)

//  const apolloProvider = new VueApollo({
//    defaultClient: new ApolloClient({
//      uri: "https://api.graphcms.com/simple/v1/awesomtalksclone"
//    })
//  })

//  Vue.use(apolloProvider)
/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

 
// Create a new instance of Database.
const database = new VuexORM.Database()
database.register(UserAlert)
database.register(User)
database.register(Post)

// Create Vuex Store and register database through Vuex ORM.

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    plugins: [VuexORM.install(database)],
    modules: {
      store
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  })

  return Store
}
